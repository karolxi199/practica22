package com.example.karolmero4a.practica2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Pasarparametro extends AppCompatActivity {

    TextView texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasarparametro);
        texto = (TextView)findViewById(R.id.textView);

        Bundle bundle = this.getIntent().getExtras();
        texto.setText(bundle.getString("dato"));

    }
}
